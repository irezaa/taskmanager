package ir.irezaa.task.list.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.irezaa.lifecycle.ViewModelFactory
import ir.irezaa.lifecycle.ViewModelKey
import ir.irezaa.lifecycle.ViewModelProviders
import ir.irezaa.task.list.di.scope.TaskListScope
import ir.irezaa.task.list.viewmodel.TaskListViewModel

@Module
interface TaskListViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(TaskListViewModel::class)
    fun bindTaskListViewModel(input: TaskListViewModel): ViewModel

    companion object {
        @Provides
        @TaskListScope
        fun provideViewModelFactory(viewModelProviders: ViewModelProviders): ViewModelFactory {
            return ViewModelFactory(viewModelProviders)
        }
    }
}