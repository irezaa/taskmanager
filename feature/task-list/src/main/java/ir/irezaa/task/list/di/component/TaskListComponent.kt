package ir.irezaa.task.list.di.component

import dagger.Component
import dagger.android.AndroidInjectionModule
import ir.irezaa.modularizer.android.DispatcherComponent
import ir.irezaa.modularizer.di.scope.FeatureScope
import ir.irezaa.task.di.component.TaskComponent
import ir.irezaa.task.list.di.module.TaskListFragmentModule
import ir.irezaa.thread.di.component.ThreadComponent

@FeatureScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        TaskListFragmentModule::class
    ],
    dependencies = [
        ThreadComponent::class,
        TaskComponent::class
    ]
)
interface TaskListComponent : DispatcherComponent