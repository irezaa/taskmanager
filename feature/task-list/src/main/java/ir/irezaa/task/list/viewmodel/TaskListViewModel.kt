package ir.irezaa.task.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.irezaa.task.Order
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.list.di.scope.TaskListScope
import ir.irezaa.task.repository.TaskRepository
import ir.irezaa.task.request.GetTaskListRequest
import ir.irezaa.thread.Dispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@TaskListScope
class TaskListViewModel @Inject constructor(
    private val taskRepository: TaskRepository,
    private val dispatcher: Dispatcher
) : ViewModel() {
    private val _taskListDataObservable = MutableLiveData<List<TaskEntity>>(mutableListOf())
    val taskListDataObservable: MutableLiveData<List<TaskEntity>> = _taskListDataObservable


    private val _currentTaskObservable = MutableLiveData<TaskEntity?>(null)
    val currentTaskObservable: MutableLiveData<TaskEntity?> = _currentTaskObservable


    fun loadTasks() {
        viewModelScope.launch(dispatcher.iO) {
            val currentTask = taskRepository.getCurrentTask()

            val tasks = taskRepository.getTasks(GetTaskListRequest())

            val mutableTasks = tasks.toMutableList()

            currentTask?.let {
                val oldCurrentTaskPosition = mutableTasks.indexOf(it)

                if (oldCurrentTaskPosition != -1) {
                    mutableTasks.removeAt(oldCurrentTaskPosition)
                    mutableTasks.add(0,it)
                }
            }

            withContext(dispatcher.main) {
                _currentTaskObservable.value = currentTask
                _taskListDataObservable.value = mutableTasks
            }
        }
    }

    fun deleteTask(taskEntity: TaskEntity) {
        viewModelScope.launch(dispatcher.iO) {
            val result = taskRepository.deleteTask(taskEntity)

            // TODO check result
            withContext(dispatcher.main) {
                loadTasks()
            }
        }
    }

    fun setCurrentTask(taskEntity: TaskEntity) {
        viewModelScope.launch(dispatcher.iO) {
            val result = taskRepository.setCurrentTask(taskEntity)

            // TODO check result
            withContext(dispatcher.main) {
                loadTasks()
            }
        }
    }

    fun onTaskDoneChanged(taskEntity: TaskEntity , newValue : Boolean){
        viewModelScope.launch(dispatcher.iO) {

            if (newValue) {
                val res = taskRepository.doneTask(taskEntity)
            }else {
                val res = taskRepository.undoneTask(taskEntity)
            }
            // todo show a toast or something else
        }
    }
}