package ir.irezaa.task.list.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.list.R
import ir.irezaa.task.list.view.cell.TaskCell

class TaskListAdapter : RecyclerView.Adapter<TaskCell>() {
    var currentTaskID = Long.MIN_VALUE
    private val taskList = mutableListOf<TaskEntity>()

    var cellCheckChangeDelegate: TaskCellCheckChangeDelegate = null
    var cellMoreClickDelegate: TaskCellMoreClickDelegate = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskCell {
        return TaskCell(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cell_task, parent, false),
            cellCheckChangeDelegate,
            cellMoreClickDelegate
        )
    }

    override fun onBindViewHolder(holder: TaskCell, position: Int) {
        val task = taskList[position]
        holder.bind(task, task.taskID == currentTaskID)
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    fun setCurrentTask(currentTask : TaskEntity?) {
        currentTaskID = currentTask?.taskID ?: Long.MIN_VALUE
    }

    fun update(newTasks: List<TaskEntity>) {
        val oldTasks: List<TaskEntity> = ArrayList(this.taskList)
        val diffResult = DiffUtil.calculateDiff(
            TaskDiffCallback(oldTasks, newTasks),
            true
        )

        taskList.clear()
        taskList.addAll(newTasks)

        diffResult.dispatchUpdatesTo(this)
    }
}

private class TaskDiffCallback(
    private val oldItemList: List<TaskEntity>,
    private val newItemList: List<TaskEntity>
) : DiffUtil.Callback() {
    override fun getOldListSize() = oldItemList.size

    override fun getNewListSize() = newItemList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItemList[oldItemPosition]
        val newItem = newItemList[newItemPosition]

        return oldItem === newItem
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItemList[oldItemPosition]
        val newItem = newItemList[newItemPosition]

        return oldItem == newItem
    }
}

typealias TaskCellCheckChangeDelegate = ((task: TaskEntity, newValue: Boolean) -> Unit)?
typealias TaskCellMoreClickDelegate = ((view: View, task: TaskEntity) -> Unit)?