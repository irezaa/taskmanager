package ir.irezaa.task.list.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.irezaa.task.list.di.scope.TaskListScope
import ir.irezaa.task.list.view.fragment.TaskListFragment

@Module
interface TaskListFragmentModule {
    @TaskListScope
    @ContributesAndroidInjector(
        modules = [TaskListViewModelModule::class]
    )
    fun bindTaskListFragment(): TaskListFragment
}