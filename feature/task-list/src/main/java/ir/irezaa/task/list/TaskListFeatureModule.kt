package ir.irezaa.task.list

import ir.irezaa.modularizer.Module
import ir.irezaa.modularizer.android.AndroidInjectorModule
import ir.irezaa.task.TaskDataModule
import ir.irezaa.task.list.di.component.DaggerTaskListComponent
import ir.irezaa.task.list.di.component.TaskListComponent
import ir.irezaa.thread.ThreadModule

object TaskListFeatureModule : AndroidInjectorModule<TaskListComponent>() {
    override fun dependencies(): List<Module> {
        return listOf(TaskDataModule,ThreadModule)
    }

    override fun initializeComponent(): TaskListComponent {
        return DaggerTaskListComponent.builder()
            .threadComponent(ThreadModule.component)
            .taskComponent(TaskDataModule.component)
            .build()
    }
}