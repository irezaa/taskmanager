package ir.irezaa.task.list.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import ir.irezaa.lifecycle.ViewModelFactory
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.list.TaskListFeatureModule
import ir.irezaa.task.list.view.adapter.TaskListAdapter
import ir.irezaa.task.list.viewmodel.TaskListViewModel
import javax.inject.Inject
import androidx.appcompat.widget.PopupMenu
import ir.irezaa.task.list.R


class TaskListFragment : Fragment(R.layout.fragment_task_list) {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: TaskListViewModel

    private lateinit var tasksRecyclerView: RecyclerView
    private lateinit var tasksAdapter: TaskListAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)

        TaskListFeatureModule.androidInjector().inject(this)

        viewModel = viewModelFactory.create(TaskListViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tasksRecyclerView = view.findViewById(R.id.tasksRecyclerView)
        tasksAdapter = TaskListAdapter()
        tasksAdapter.cellCheckChangeDelegate = viewModel::onTaskDoneChanged
        tasksAdapter.cellMoreClickDelegate = this::showPopupMenu

        tasksRecyclerView.apply {
            adapter = tasksAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        view.findViewById<FloatingActionButton>(R.id.newTaskFloatingButton).setOnClickListener {
            findNavController().navigate(
                requireContext().getString(R.string.deeplink_task_create).toUri()
            )
        }

        viewModel.apply {
            taskListDataObservable.observe(viewLifecycleOwner, tasksAdapter::update)
            currentTaskObservable.observe(viewLifecycleOwner,tasksAdapter::setCurrentTask)
            loadTasks()
        }
    }

    private fun showPopupMenu(view: View, taskEntity: TaskEntity) {
        val popup = PopupMenu(requireContext(), view)
        popup.inflate(R.menu.cell_task_menu)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.cell_task_menu_deleteItem -> {
                    viewModel.deleteTask(taskEntity)
                }
                R.id.cell_task_menu_setAsCurrent -> {
                    viewModel.setCurrentTask(taskEntity)
                }
            }
            true
        }
        popup.show()
    }
}