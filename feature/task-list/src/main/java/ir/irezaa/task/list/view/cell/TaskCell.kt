package ir.irezaa.task.list.view.cell

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textview.MaterialTextView
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.list.R
import ir.irezaa.task.list.view.adapter.TaskCellCheckChangeDelegate
import ir.irezaa.task.list.view.adapter.TaskCellMoreClickDelegate

class TaskCell(
    itemView: View,
    private val checkChangeDelegate: TaskCellCheckChangeDelegate,
    private val moreButtonClickDelegate: TaskCellMoreClickDelegate
) : RecyclerView.ViewHolder(itemView) {
    private val backgroundContainer: ConstraintLayout =
        itemView.findViewById(R.id.cell_task_backgroundContainer)
    private val doneCheckBox: MaterialCheckBox = itemView.findViewById(R.id.cell_task_doneCheckBox)
    private val moreButton: AppCompatImageView = itemView.findViewById(R.id.cell_task_moreButton)
    private val titleTextView: MaterialTextView = itemView.findViewById(R.id.cell_task_titleText)
    private val descTextView: MaterialTextView = itemView.findViewById(R.id.cell_task_descText)
    private val priorityTextView: MaterialTextView =
        itemView.findViewById(R.id.cell_task_priorityText)

    private lateinit var currentTaskEntity: TaskEntity

    init {
        doneCheckBox.setOnClickListener {
            checkChangeDelegate?.invoke(currentTaskEntity, currentTaskEntity.done.not())
        }
        moreButton.setOnClickListener {
            moreButtonClickDelegate?.invoke(it, currentTaskEntity)
        }
    }

    fun bind(taskEntity: TaskEntity, isCurrent: Boolean) {
        currentTaskEntity = taskEntity

        if (isCurrent) {
            backgroundContainer.setBackgroundColor(
                backgroundContainer.context.resources.getColor(R.color.surface_color_1)
            )
        } else {
            backgroundContainer.background = null
        }

        titleTextView.text = taskEntity.title
        descTextView.text = taskEntity.desc
        priorityTextView.text = taskEntity.order.name
        doneCheckBox.isChecked = taskEntity.done
    }
}