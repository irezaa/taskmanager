package ir.irezaa.task.add.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.irezaa.task.Order
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.add.di.scope.AddTaskScope
import ir.irezaa.task.repository.TaskRepository
import ir.irezaa.thread.Dispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AddTaskScope
class AddTaskViewModel @Inject constructor(
    private val taskRepository: TaskRepository,
    private val dispatcher : Dispatcher
) : ViewModel() {

    private val _navigateUpObservable = MutableLiveData<Unit>()
    val navigateUpObservable : LiveData<Unit> = _navigateUpObservable

    fun addTask(title: String, desc: String, order : Order) {
        // TODO validate fields

        viewModelScope.launch(dispatcher.iO) {
            val result = taskRepository.addTask(TaskEntity(
                title = title,
                desc = desc,
                order = order,
                done = false
            ))

            // TODO check result for error

            withContext(dispatcher.main) {
                _navigateUpObservable.value = Unit
            }
        }

    }
}