package ir.irezaa.task.add.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import ir.irezaa.lifecycle.ViewModelFactory
import ir.irezaa.task.Order
import ir.irezaa.task.add.AddTaskFeatureModule
import ir.irezaa.task.add.R
import ir.irezaa.task.add.viewmodel.AddTaskViewModel
import javax.inject.Inject

class AddTaskFragment : Fragment(R.layout.fragment_addtask) {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: AddTaskViewModel


    override fun onAttach(context: Context) {
        super.onAttach(context)
        AddTaskFeatureModule.androidInjector().inject(this)
        viewModel = viewModelFactory.create(AddTaskViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val titleField : TextInputEditText = view.findViewById(R.id.title_input_field)
        val descField : TextInputEditText = view.findViewById(R.id.desc_input_field)
        val floatingDoneButton : FloatingActionButton = view.findViewById(R.id.newTaskFloatingButton)

        floatingDoneButton.setOnClickListener {
            viewModel.addTask(titleField.text.toString(),descField.text.toString(),Order.High)
        }

        viewModel.apply {
            navigateUpObservable.observe(viewLifecycleOwner) {
                findNavController().navigateUp()
            }
        }
    }
}