package ir.irezaa.task.add.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.irezaa.lifecycle.ViewModelFactory
import ir.irezaa.lifecycle.ViewModelKey
import ir.irezaa.lifecycle.ViewModelProviders
import ir.irezaa.task.add.di.scope.AddTaskScope
import ir.irezaa.task.add.viewmodel.AddTaskViewModel

@Module
interface AddTaskViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AddTaskViewModel::class)
    fun bindAddTaskViewModel(input: AddTaskViewModel): ViewModel

    companion object {
        @Provides
        @AddTaskScope
        fun provideViewModelFactory(viewModelProviders: ViewModelProviders): ViewModelFactory {
            return ViewModelFactory(viewModelProviders)
        }
    }
}