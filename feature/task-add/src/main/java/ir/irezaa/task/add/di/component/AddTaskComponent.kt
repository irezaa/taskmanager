package ir.irezaa.task.add.di.component

import dagger.Component
import dagger.android.AndroidInjectionModule
import ir.irezaa.modularizer.android.DispatcherComponent
import ir.irezaa.modularizer.di.scope.FeatureScope
import ir.irezaa.task.add.di.module.AddTaskFragmentModule
import ir.irezaa.task.di.component.TaskComponent
import ir.irezaa.thread.di.component.ThreadComponent

@FeatureScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AddTaskFragmentModule::class
    ],
    dependencies = [
        ThreadComponent::class,
        TaskComponent::class
    ]
)
interface AddTaskComponent : DispatcherComponent