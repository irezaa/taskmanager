package ir.irezaa.task.add.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.irezaa.task.add.di.scope.AddTaskScope
import ir.irezaa.task.add.view.fragment.AddTaskFragment

@Module
interface AddTaskFragmentModule {
    @AddTaskScope
    @ContributesAndroidInjector(
        modules = [AddTaskViewModelModule::class]
    )
    fun bindAddTaskFragmentFragment(): AddTaskFragment
}