package ir.irezaa.task.add

import ir.irezaa.modularizer.Module
import ir.irezaa.modularizer.android.AndroidInjectorModule
import ir.irezaa.task.TaskDataModule
import ir.irezaa.task.add.di.component.AddTaskComponent
import ir.irezaa.task.add.di.component.DaggerAddTaskComponent
import ir.irezaa.thread.ThreadModule

object AddTaskFeatureModule : AndroidInjectorModule<AddTaskComponent>() {
    override fun dependencies(): List<Module> {
        return listOf(TaskDataModule,ThreadModule)
    }

    override fun initializeComponent(): AddTaskComponent {
        return DaggerAddTaskComponent.builder()
            .taskComponent(TaskDataModule.component)
            .threadComponent(ThreadModule.component)
            .build()
    }
}