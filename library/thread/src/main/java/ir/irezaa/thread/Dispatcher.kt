package ir.irezaa.thread

import kotlinx.coroutines.CoroutineDispatcher

data class Dispatcher(
    val iO: CoroutineDispatcher,
    val main: CoroutineDispatcher
)
