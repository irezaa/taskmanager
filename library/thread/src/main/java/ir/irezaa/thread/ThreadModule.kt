package ir.irezaa.thread

import ir.irezaa.modularizer.di.DaggerModule
import ir.irezaa.thread.di.component.DaggerThreadComponent
import ir.irezaa.thread.di.component.ThreadComponent

object ThreadModule : DaggerModule<ThreadComponent>() {
    override fun initializeComponent(): ThreadComponent {
        return DaggerThreadComponent.create()
    }
}