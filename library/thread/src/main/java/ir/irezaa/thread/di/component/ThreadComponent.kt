package ir.irezaa.thread.di.component

import ir.irezaa.thread.Dispatcher
import ir.irezaa.thread.di.module.ThreadModule
import dagger.Component
import ir.irezaa.modularizer.di.DaggerComponent
import ir.irezaa.modularizer.di.scope.LibraryScope

@LibraryScope
@Component(modules = [ThreadModule::class])
interface ThreadComponent : DaggerComponent {

    fun exposeDispatcher(): Dispatcher
}