package ir.irezaa.thread.di.module

import ir.irezaa.thread.Dispatcher
import dagger.Module
import dagger.Provides
import ir.irezaa.modularizer.di.scope.LibraryScope
import kotlinx.coroutines.Dispatchers

@Module
class ThreadModule {

    @Provides
    @LibraryScope
    fun provideDispatcher(): Dispatcher {
        return Dispatcher(iO = Dispatchers.IO, main = Dispatchers.Main)
    }
}