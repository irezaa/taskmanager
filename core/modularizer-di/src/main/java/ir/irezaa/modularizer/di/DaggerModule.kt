package ir.irezaa.modularizer.di

import ir.irezaa.modularizer.Module

abstract class DaggerModule<Component : DaggerComponent> : Module {
    private var _component: Component? = null

    val component: Component
        get() {
            return _component!!
        }

    override fun onStart() {
        if (_component == null) {
            _component = initializeComponent()
        }
    }

    abstract fun initializeComponent(): Component
}