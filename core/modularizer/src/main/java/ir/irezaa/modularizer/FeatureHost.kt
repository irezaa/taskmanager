package ir.irezaa.modularizer

interface FeatureHost {
    fun installFeature(module: Module)
}