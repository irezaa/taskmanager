package ir.irezaa.modularizer

interface Module {
    fun dependencies(): List<Module>? {
        return null
    }

    fun onStart()
}