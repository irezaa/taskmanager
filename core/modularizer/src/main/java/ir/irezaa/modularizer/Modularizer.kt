package ir.irezaa.modularizer

import kotlin.reflect.KClass

open class Modularizer : FeatureHost {
    private val modules = mutableMapOf<KClass<Module>, Module>()
    private val installingModules = hashSetOf<KClass<Module>>()

    fun startFeatures() {
        if (modules.isEmpty()) {
            return
        }

        modules.values.forEach { it ->
            it.onStart()
        }
    }

    override fun installFeature(module : Module) {
        if (installingModules.contains(module::class) || modules.containsKey(module::class)) {
            return
        }
        installingModules.add(module.javaClass.kotlin)

        module.dependencies()?.forEach {
            installFeature(it)
        }

        modules[module.javaClass.kotlin] = module
        installingModules.remove(module.javaClass.kotlin)
    }
}