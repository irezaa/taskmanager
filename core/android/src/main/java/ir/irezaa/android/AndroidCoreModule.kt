package ir.irezaa.android

import android.app.Application
import ir.irezaa.android.di.component.AndroidCoreComponent
import ir.irezaa.android.di.component.DaggerAndroidCoreComponent
import ir.irezaa.modularizer.android.ApplicationComponentBuilder
import ir.irezaa.modularizer.di.DaggerModule

object AndroidCoreModule : DaggerModule<AndroidCoreComponent>(), ApplicationComponentBuilder {
    override lateinit var application: Application

    override fun initializeComponent(): AndroidCoreComponent {
        return DaggerAndroidCoreComponent
            .builder()
            .bindApplication(application)
            .build()
    }
}