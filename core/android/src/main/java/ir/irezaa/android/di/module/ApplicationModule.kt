package ir.irezaa.android.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import ir.irezaa.android.di.qualifier.ApplicationContext
import ir.irezaa.android.di.scope.AndroidScope

@Module
class ApplicationModule {

    @Provides
    @ApplicationContext
    @AndroidScope
    fun provideApplicationContext(application: Application): Context {
        return application
    }
}