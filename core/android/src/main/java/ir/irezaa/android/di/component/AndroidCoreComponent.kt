package ir.irezaa.android.di.component

import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ir.irezaa.android.di.module.ApplicationModule
import ir.irezaa.android.di.qualifier.ApplicationContext
import ir.irezaa.android.di.scope.AndroidScope
import ir.irezaa.modularizer.di.DaggerComponent

@AndroidScope
@Component(modules = [ApplicationModule::class])
interface AndroidCoreComponent : DaggerComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindApplication(application: Application): Builder
        fun build(): AndroidCoreComponent
    }

    fun exposeApplication(): Application

    @ApplicationContext
    fun exposeApplicationContext(): Context
}