package ir.irezaa.modularizer.android

import android.app.Application
import ir.irezaa.modularizer.Module
import ir.irezaa.modularizer.Modularizer

abstract class DaggerModularApplication : Application() {
    protected abstract val applicationComponentBuilder: ApplicationComponentBuilder

    private val modularizer = Modularizer()

    final override fun onCreate() {
        super.onCreate()
        applicationComponentBuilder.application = this
        onStart()
        modularizer.startFeatures()
    }

    abstract fun onStart()

    protected fun <T : DispatcherComponent> installFeature(module : AndroidInjectorModule<T>) {
        modularizer.installFeature(module)
    }
}