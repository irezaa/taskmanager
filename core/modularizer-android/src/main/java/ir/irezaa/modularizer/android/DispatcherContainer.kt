package ir.irezaa.modularizer.android

import dagger.android.AndroidInjector

interface DispatcherContainer {

    fun androidInjector(): AndroidInjector<Any>
}