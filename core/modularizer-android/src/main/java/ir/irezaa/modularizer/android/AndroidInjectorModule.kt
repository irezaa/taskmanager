package ir.irezaa.modularizer.android

import dagger.android.AndroidInjector
import ir.irezaa.modularizer.di.DaggerModule

abstract class AndroidInjectorModule<T : DispatcherComponent> : DaggerModule<T>(), DispatcherContainer {
    override fun androidInjector(): AndroidInjector<Any> {
        return component.dispatcher()
    }
}