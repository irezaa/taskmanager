package ir.irezaa.modularizer.android

import android.app.Application

interface ApplicationComponentBuilder {
    var application : Application
}