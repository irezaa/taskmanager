package ir.irezaa.modularizer.android

import dagger.android.DispatchingAndroidInjector
import ir.irezaa.modularizer.di.DaggerComponent

interface DispatcherComponent : DaggerComponent {

    fun dispatcher(): DispatchingAndroidInjector<Any>
}