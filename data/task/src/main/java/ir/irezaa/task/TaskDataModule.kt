package ir.irezaa.task

import ir.irezaa.android.AndroidCoreModule
import ir.irezaa.modularizer.Module
import ir.irezaa.modularizer.di.DaggerModule
import ir.irezaa.task.di.component.DaggerTaskComponent
import ir.irezaa.task.di.component.TaskComponent

object TaskDataModule : DaggerModule<TaskComponent>() {
    override fun dependencies(): List<Module> {
        return listOf(AndroidCoreModule)
    }
    override fun initializeComponent(): TaskComponent {
        return DaggerTaskComponent.builder()
            .androidCoreComponent(AndroidCoreModule.component)
            .build()
    }
}