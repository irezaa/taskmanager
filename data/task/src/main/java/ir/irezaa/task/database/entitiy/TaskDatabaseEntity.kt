package ir.irezaa.task.database.entitiy

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks_table")
internal data class TaskDatabaseEntity(
    val title: String,
    val desc : String,
    val order : Int,
    val done : Boolean
) {
    @PrimaryKey(autoGenerate = true)
    var taskID: Long = 0
}