package ir.irezaa.task.error

sealed class TaskDataError : Throwable() {

    class InsertionFailed : TaskDataError()

    class DeletionFailed : TaskDataError()

    class UpdateFailed : TaskDataError()

    class UndefinedAction : TaskDataError()
}