package ir.irezaa.task.di.module

import dagger.Binds
import dagger.Module
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.datasource.*

@Module
internal interface TaskDataSourceModule {
    @Binds
    @DataScope
    fun bindTaskItemDatabaseDataSourceToWritable(input: TaskItemDatabaseDataSource)
            : TaskItemDSWritable

    @Binds
    @DataScope
    fun bindTaskItemDatabaseDataSourceToReadable(input: TaskItemDatabaseDataSource)
            : TaskItemDSReadable

    @Binds
    @DataScope
    fun bindTaskItemDatabaseDataSourceToUpdatable(input: TaskItemDatabaseDataSource)
            : TaskItemDSUpdatable

    @Binds
    @DataScope
    fun bindTaskItemDatabaseDataSourceToDeletable(input: TaskItemDatabaseDataSource)
            : TaskItemDSDeletable


    @Binds
    @DataScope
    fun bindTaskListDatabaseDataSourceToReadable(input: TaskListDatabaseDataSource)
            : TaskListDSReadable

    @Binds
    @DataScope
    fun bindCurrentTaskDataSourceAsReadable(input: CurrentTaskDataSource)
            : CurrentTaskReadable

    @Binds
    @DataScope
    fun bindCurrentTaskDataSourceAsWritable(input: CurrentTaskDataSource)
            : CurrentTaskWritable
}