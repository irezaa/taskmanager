package ir.irezaa.task.repository

import ir.irezaa.funtional.Response
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.error.TaskDataError
import ir.irezaa.task.request.GetTaskListRequest

interface TaskRepository {
    suspend fun getTasks(taskListRequest: GetTaskListRequest) : List<TaskEntity>
    suspend fun getCurrentTask() : TaskEntity?
    suspend fun setCurrentTask(taskEntity: TaskEntity)
    suspend fun doneTask(taskEntity: TaskEntity) : Response<Unit, TaskDataError>
    suspend fun addTask(taskEntity: TaskEntity) : Response<Unit, TaskDataError>
    suspend fun undoneTask(taskEntity: TaskEntity) : Response<Unit, TaskDataError>
    suspend fun deleteTask(taskEntity: TaskEntity) : Response<Unit, TaskDataError>
}