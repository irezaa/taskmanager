package ir.irezaa.task

data class TaskEntity(
    val title: String,
    val desc: String,
    val order: Order,
    var done: Boolean
) {
    var taskID: Long = 0
}