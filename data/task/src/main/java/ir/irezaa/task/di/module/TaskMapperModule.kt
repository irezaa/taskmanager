package ir.irezaa.task.di.module

import dagger.Binds
import dagger.Module
import ir.irezaa.mapper.Mapper
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.database.entitiy.TaskDatabaseEntity
import ir.irezaa.task.datasource.TaskItemDatabaseDataSource
import ir.irezaa.task.mapper.TaskDatabaseEntityToEntityMapper

@Module
internal interface TaskMapperModule {
    @Binds
    @DataScope
    fun bindTaskDatabaseEntityToEntityMapper(input: TaskDatabaseEntityToEntityMapper)
            : Mapper<TaskDatabaseEntity, TaskEntity>
}