package ir.irezaa.task.di.module

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ir.irezaa.android.di.qualifier.ApplicationContext
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.database.TaskDatabase
import ir.irezaa.task.database.TaskEntityDao

@Module
internal class TaskDatabaseModule {
    @Provides
    @DataScope
    fun provideTaskDatabase(@ApplicationContext context: Context): TaskDatabase {
        return Room.databaseBuilder(context, TaskDatabase::class.java, "task_database")
            .build()
    }

    @Provides
    @DataScope
    fun provideTaskEntityDao(taskDatabase: TaskDatabase): TaskEntityDao {
        return taskDatabase.tasksEntityDao()
    }

    @Provides
    @DataScope
    fun provideSharedPreferences(@ApplicationContext context: Context) : SharedPreferences {
        return context.getSharedPreferences("tasks",Context.MODE_PRIVATE)
    }
}