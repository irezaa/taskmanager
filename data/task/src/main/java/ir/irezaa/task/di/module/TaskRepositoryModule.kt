package ir.irezaa.task.di.module

import dagger.Binds
import dagger.Module
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.repository.TaskRepository
import ir.irezaa.task.repository.TaskRepositoryImp

@Module
internal interface TaskRepositoryModule {
    @Binds
    @DataScope
    fun bindTaskRepositoryImpToTaskRepository(input: TaskRepositoryImp)
            : TaskRepository
}