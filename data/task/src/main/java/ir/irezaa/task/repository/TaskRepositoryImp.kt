package ir.irezaa.task.repository

import ir.irezaa.funtional.Response
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.datasource.*
import ir.irezaa.task.error.TaskDataError
import ir.irezaa.task.request.GetTaskListRequest
import javax.inject.Inject

@DataScope
class TaskRepositoryImp @Inject constructor(
    private val currentTaskReadable: CurrentTaskReadable,
    private val currentTaskWritable: CurrentTaskWritable,
    private val taskListDSReadable: TaskListDSReadable,
    private val taskItemReadable: TaskItemDSReadable,
    private val taskItemWritable: TaskItemDSWritable,
    private val taskItemUpdatable: TaskItemDSUpdatable,
    private val taskItemDeletable: TaskItemDSDeletable
) : TaskRepository {

    override suspend fun getTasks(taskListRequest: GetTaskListRequest): List<TaskEntity> {
        return taskListDSReadable.read(taskListRequest)
    }

    override suspend fun getCurrentTask(): TaskEntity? {
        val currentTaskID = currentTaskReadable.read()

        if (currentTaskID == Long.MIN_VALUE) {
            return null
        }

        return taskItemReadable.read(currentTaskID)
    }

    override suspend fun setCurrentTask(taskEntity: TaskEntity) {
        currentTaskWritable.write(taskEntity.taskID)
    }

    override suspend fun doneTask(taskEntity: TaskEntity): Response<Unit, TaskDataError> {
        taskEntity.done = true
        return taskItemUpdatable.update(taskEntity)
    }

    override suspend fun undoneTask(taskEntity: TaskEntity): Response<Unit, TaskDataError> {
        taskEntity.done = false
        return taskItemUpdatable.update(taskEntity)
    }

    override suspend fun addTask(taskEntity: TaskEntity): Response<Unit, TaskDataError> {
        return taskItemWritable.write(taskEntity)
    }

    override suspend fun deleteTask(taskEntity: TaskEntity): Response<Unit, TaskDataError> {
        val currentTaskID = currentTaskReadable.read()

        if (taskEntity.taskID == currentTaskID) {
            currentTaskWritable.write(Long.MIN_VALUE)
        }

        return taskItemDeletable.delete(taskEntity)
    }
}