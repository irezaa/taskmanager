package ir.irezaa.task.database.dao

import androidx.room.*
import ir.irezaa.task.database.TaskEntityDao
import ir.irezaa.task.database.entitiy.TaskDatabaseEntity

@Dao
internal interface TaskRoomEntityDao : TaskEntityDao {

    @Query("SELECT * FROM tasks_table WHERE taskID = :id LIMIT 1")
    override suspend fun getTask(id : Long): TaskDatabaseEntity

    @Insert(entity = TaskDatabaseEntity::class, onConflict = OnConflictStrategy.REPLACE)
    override suspend fun insertTask(task: TaskDatabaseEntity): Long

    @Query("SELECT * FROM tasks_table ORDER BY `order` DESC,`taskID` DESC")
    override suspend fun getAll(): List<TaskDatabaseEntity>

    @Insert(entity = TaskDatabaseEntity::class, onConflict = OnConflictStrategy.REPLACE)
    override suspend fun insertTasks(taskList: List<TaskDatabaseEntity>): Array<Long>

    @Delete(entity = TaskDatabaseEntity::class)
    override suspend fun deleteTask(task: TaskDatabaseEntity): Int

    @Delete(entity = TaskDatabaseEntity::class)
    override suspend fun deleteTasks(tasks: List<TaskDatabaseEntity>): Int

    @Update(entity = TaskDatabaseEntity::class)
    override suspend fun updateTask(task: TaskDatabaseEntity): Int
}