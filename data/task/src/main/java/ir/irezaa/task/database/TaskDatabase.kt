package ir.irezaa.task.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.irezaa.task.database.dao.TaskRoomEntityDao
import ir.irezaa.task.database.entitiy.TaskDatabaseEntity

@Database(entities = [TaskDatabaseEntity::class], version = 1)
internal abstract class TaskDatabase : RoomDatabase() {

    abstract fun tasksEntityDao(): TaskRoomEntityDao
}