package ir.irezaa.task.database

import ir.irezaa.task.database.entitiy.TaskDatabaseEntity

internal interface TaskEntityDao {
    suspend fun getTask(id : Long) : TaskDatabaseEntity
    suspend fun insertTask(task: TaskDatabaseEntity): Long
    suspend fun getAll(): List<TaskDatabaseEntity>
    suspend fun insertTasks(taskList: List<TaskDatabaseEntity>): Array<Long>
    suspend fun deleteTask(task: TaskDatabaseEntity): Int
    suspend fun deleteTasks(tasks: List<TaskDatabaseEntity>): Int
    suspend fun updateTask(task: TaskDatabaseEntity): Int
}