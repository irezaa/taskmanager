package ir.irezaa.task.di.component

import dagger.Component
import ir.irezaa.android.di.component.AndroidCoreComponent
import ir.irezaa.modularizer.di.DaggerComponent
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.di.module.TaskDataSourceModule
import ir.irezaa.task.di.module.TaskDatabaseModule
import ir.irezaa.task.di.module.TaskMapperModule
import ir.irezaa.task.di.module.TaskRepositoryModule
import ir.irezaa.task.repository.TaskRepository

@DataScope
@Component(
    modules = [
        TaskRepositoryModule::class,
        TaskDataSourceModule::class,
        TaskMapperModule::class,
        TaskDatabaseModule::class
    ],
    dependencies = [
        AndroidCoreComponent::class
    ]
)
interface TaskComponent : DaggerComponent {
    fun exposeTaskRepository() : TaskRepository
}