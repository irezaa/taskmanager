package ir.irezaa.task.mapper

import ir.irezaa.mapper.Mapper
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.Order
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.database.entitiy.TaskDatabaseEntity
import javax.inject.Inject

@DataScope
internal class TaskDatabaseEntityToEntityMapper @Inject constructor() :
    Mapper<TaskDatabaseEntity, TaskEntity> {
    override fun mapFirstToSecond(first: TaskDatabaseEntity): TaskEntity {
        val taskEntity = TaskEntity(
            title = first.title,
            desc = first.desc,
            order = Order.fromRawValue(first.order),
            done = first.done
        )
        taskEntity.taskID = first.taskID
        return taskEntity
    }

    override fun mapSecondToFirst(second: TaskEntity): TaskDatabaseEntity {
        val entity = TaskDatabaseEntity(
            title = second.title,
            desc = second.desc,
            order = second.order.rawValue,
            done = second.done
        )
        entity.taskID = second.taskID
        return entity
    }
}