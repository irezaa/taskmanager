package ir.irezaa.task.datasource

import android.content.SharedPreferences
import ir.irezaa.datasource.Readable
import ir.irezaa.datasource.Writable
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.TaskEntity
import javax.inject.Inject

@DataScope
internal class CurrentTaskDataSource @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) : CurrentTaskReadable,
    CurrentTaskWritable {

    override fun read(): Long {
        return sharedPreferences.getLong(CURRENT_TASK_KEY, Long.MIN_VALUE)
    }

    override fun write(input: Long) {
        sharedPreferences.edit().apply {
            putLong(CURRENT_TASK_KEY, input)
            apply()
        }
    }
}

private const val CURRENT_TASK_KEY = "CURRENT_TASK"

typealias CurrentTaskReadable = Readable<Long>
typealias CurrentTaskWritable = Writable<Long>