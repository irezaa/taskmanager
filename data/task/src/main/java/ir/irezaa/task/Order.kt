package ir.irezaa.task

enum class Order(val rawValue: Int) {
    High(2),
    Normal(1),
    Low(0);

    companion object {
        fun fromRawValue(value: Int) = Order.values().first { it.rawValue == value }
    }
}