package ir.irezaa.task.datasource

import ir.irezaa.datasource.Readable
import ir.irezaa.mapper.Mapper
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.database.TaskEntityDao
import ir.irezaa.task.database.entitiy.TaskDatabaseEntity
import ir.irezaa.task.request.GetTaskListRequest
import javax.inject.Inject

@DataScope
internal class TaskListDatabaseDataSource @Inject constructor(
    private val dao: TaskEntityDao,
    private val mapper: Mapper<TaskDatabaseEntity, TaskEntity>
) : TaskListDSReadable{

    override suspend fun read(input: GetTaskListRequest): List<TaskEntity> {
        return dao.getAll()
            .map(mapper::mapFirstToSecond)
    }
}

typealias TaskListDSReadable = Readable.Suspendable.IO<GetTaskListRequest,
        @JvmSuppressWildcards List<TaskEntity>>