package ir.irezaa.task.datasource

import ir.irezaa.datasource.Deletable
import ir.irezaa.datasource.Readable
import ir.irezaa.datasource.Updatable
import ir.irezaa.datasource.Writable
import ir.irezaa.funtional.Failure
import ir.irezaa.funtional.Response
import ir.irezaa.funtional.Success
import ir.irezaa.mapper.Mapper
import ir.irezaa.modularizer.di.scope.DataScope
import ir.irezaa.task.TaskEntity
import ir.irezaa.task.database.TaskEntityDao
import ir.irezaa.task.database.entitiy.TaskDatabaseEntity
import ir.irezaa.task.error.TaskDataError
import javax.inject.Inject

@DataScope
internal class TaskItemDatabaseDataSource @Inject constructor(
    private val dao: TaskEntityDao,
    private val mapper: Mapper<TaskDatabaseEntity, TaskEntity>
) : TaskItemDSReadable,
    TaskItemDSWritable,
    TaskItemDSUpdatable,
    TaskItemDSDeletable{

    override suspend fun read(input: Long): TaskEntity {
        val taskDatabaseEntity = dao.getTask(input)
        return mapper.mapFirstToSecond(taskDatabaseEntity)
    }

    override suspend fun write(input: TaskEntity): Response<Unit, TaskDataError> {
        val taskDatabaseEntity = mapper.mapSecondToFirst(input)
        val affectedRows = dao.insertTask(taskDatabaseEntity)
        return if (affectedRows >= 0) {
            Success(Unit)
        } else {
            Failure(TaskDataError.InsertionFailed())
        }
    }

    override suspend fun update(input: TaskEntity): Response<Unit, TaskDataError> {
        val taskDatabaseEntity = mapper.mapSecondToFirst(input)
        val affectedRows = dao.updateTask(taskDatabaseEntity)
        return if (affectedRows > 0) {
            Success(Unit)
        } else {
            Failure(TaskDataError.UpdateFailed())
        }
    }

    override suspend fun delete(input: TaskEntity): Response<Unit, TaskDataError> {
        val taskDatabaseEntity = mapper.mapSecondToFirst(input)
        val affectedRows = dao.deleteTask(taskDatabaseEntity)
        return if (affectedRows > 0) {
            Success(Unit)
        } else {
            Failure(TaskDataError.DeletionFailed())
        }
    }
}

typealias TaskItemDSReadable = Readable.Suspendable.IO<Long,
        @JvmSuppressWildcards TaskEntity>

typealias TaskItemDSWritable = Writable.Suspendable.IO<TaskEntity,
        @JvmSuppressWildcards Response<Unit, TaskDataError>>

typealias TaskItemDSUpdatable = Updatable.Suspendable.IO<TaskEntity,
        @JvmSuppressWildcards Response<Unit, TaskDataError>>

typealias TaskItemDSDeletable = Deletable.Suspendable.IO<TaskEntity,
        @JvmSuppressWildcards Response<Unit, TaskDataError>>