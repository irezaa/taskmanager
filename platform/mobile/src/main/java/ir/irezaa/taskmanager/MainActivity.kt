package ir.irezaa.taskmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.irezaa.task.list.view.fragment.TaskListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}