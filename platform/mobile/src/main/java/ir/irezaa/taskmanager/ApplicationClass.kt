package ir.irezaa.taskmanager

import android.content.Context
import androidx.multidex.MultiDex
import ir.irezaa.android.AndroidCoreModule
import ir.irezaa.modularizer.android.ApplicationComponentBuilder
import ir.irezaa.modularizer.android.DaggerModularApplication
import ir.irezaa.task.add.AddTaskFeatureModule
import ir.irezaa.task.list.TaskListFeatureModule

class ApplicationClass : DaggerModularApplication() {
    override val applicationComponentBuilder: ApplicationComponentBuilder
        get() = AndroidCoreModule

    override fun onStart() {
        installFeature(TaskListFeatureModule)
        installFeature(AddTaskFeatureModule)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}